﻿using System;

namespace Fabula_Liebre_Tortuga
{
    class Tortuga
    {
        private int meta;
        private int step;

        public Tortuga(int meta) {
            this.step = 0;
            this.meta = meta;
        }

        public void run() {
            if (!this.isFinished()){
                this.doStep();
                Console.WriteLine("La tortuga avanza 1 mts., esta en: {0}/{1} mts.",this.step, this.meta);
            }
            else{
                Console.WriteLine("La tortuga ha llegado");
            }

        }
        public void doStep(){
            this.step += 1;
        }

        public void wait() {
            Console.WriteLine("La tortuga ha descansado un momento.");
        }
    

        public bool isFinished() {
            return this.step == this.meta;
        }
        
    }
}
