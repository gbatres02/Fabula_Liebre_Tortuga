﻿using System;
using System.Threading;

namespace Fabula_Liebre_Tortuga
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("La carrera de la tortuga y el conejo");
            int meta = 0;
            Console.WriteLine("Ingrese los metros a recorrer en la carrera: (Solo enteros)");
            String data = Console.ReadLine();
            if (data == "" || data.Contains(" "))
            {
               Console.WriteLine("El valor no es valido");
               Console.ReadLine();
            }
            else
            {
                meta = Convert.ToInt32(data);
                Carrera carrera = new Carrera(meta);
                Thread t = new Thread(carrera.start);
                Console.WriteLine("Presione una tecla para continuar");
                Console.ReadLine();
                t.Start();
            }
        }
    }
}
