﻿using System;
using System.Threading;

namespace Fabula_Liebre_Tortuga
{
    class Carrera
    {
        private Tortuga tortuga;
        private Liebre conejo;
        private int meta;
        private string name_finish;

        public Carrera(int meta) {
            this.meta = meta;
            this.name_finish = "";
        }

        public void prepared() {
            this.tortuga = new Tortuga(this.meta);
            this.conejo = new Liebre(this.meta);
        }
        public void start() {
            Console.WriteLine("Preparence para la carrera");
            this.prepared();
            while (!isFinished()) {
                this.tortuga.run();
                this.conejo.run();
                Thread.Sleep(500);
            }
            Console.WriteLine("La carrera ha terminado");
            Console.ReadLine();
            Console.WriteLine("El ganador es:" + this.name_finish);
            Console.ReadLine();
        }
        public bool isFinished() {
            if (this.tortuga.isFinished()) {
                this.name_finish = " Tortuga";
                return true;
            } else if (this.conejo.isFinished()) {
                this.name_finish = " Liebre";
                return true;
            }
            return false;
        }
    }
}
