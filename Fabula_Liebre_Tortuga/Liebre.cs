﻿using System;

namespace Fabula_Liebre_Tortuga
{
    class Liebre 
    {

        private int meta;
        private int step;
        private int step_aux;
        private int meta_aux;

        public Liebre(int meta){
            this.step = 0;
            this.meta = meta;
            this.step_aux = 0;
            this.meta_aux = Convert.ToInt32(this.meta - (this.meta * .20));
        }

        public void run(){
            if (!this.isFinished()){
                if ((this.step >= (this.meta / 2)) && (this.step_aux <= this.meta_aux)){
                    Console.WriteLine("La liebre ha descansado un momento.");
                }
                else {
                    this.doStep();
                    Console.WriteLine("La liebre avanza 2 mts., esta en: {0}/{1} mts.", this.step,this.meta);
                }
                this.step_aux += 1;
            }
            else{
                Console.WriteLine("La liebre ha llegado");
            }
        }
        public void doStep(){
            this.step += 2;
        }
        

        public bool isFinished(){
            return this.step == this.meta;
        }
    }
}
